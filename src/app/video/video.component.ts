import { Component, OnInit, Renderer2 } from '@angular/core';
import { MouseService, WebSocketService } from '../service';
import { WebRTCService } from '../service';
import { SubjectsService } from '../service';



@Component({
  selector: 'video-root',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})

export class VideoComponent implements OnInit {
  private videoElement: any;
  private remoteElement: any = {};
  private videoBox: any;

  users: Array<string> = new Array();
  roomNumber: string;
  name: string;
  username: string = 'ゲスト';
  roomname: string = 'test1';
  groupname: string = 'test';

  websocketEvents: object = [
    'allusers', 'msg', 'draw', 'image', 'webrtc'
  ];

  private Reader: FileReader;
  Images = new Array();

  constructor(
    private renderer2: Renderer2,
    private websocketService: WebSocketService,
    private webrtcService: WebRTCService,
    private subjectService: SubjectsService
  ) {}


  ngOnInit(): void {
    this.hub();
    this.videoBox = document.getElementById('videoBox');
  }


  private hub(): void {

    this.subjectService.on('on_allusers')
      .subscribe((result: any) => {
        this.users = new Array<string>();
        console.log(result);
        for (const key in result) {
          if (result.hasOwnProperty(key)) {
            this.users.push(result[key]['name']);
          }
        }

      });
    this.subjectService.on('on_join')
    .subscribe((result: any) => {
      if (result['type'] === 'new_user') {
        this.subjectService.publish(
          'alert',
          result['data']['name'] + 'が参加しました'
        );
      }
    });
    this.subjectService.on('on_replicate')
      .subscribe((result: any) => {
    });
    this.subjectService.on('on_leave')
      .subscribe((result: any) => {
      this.deleteRemoteVideoElement(result['data']['id']);
      this.webrtcService.deleteConnection(result['data']['id']);
    });
    this.subjectService.on('on_webrtc')
      .subscribe((result: any) => {
        this.webrtcManager(result);
      });
  }


  /**
   *
   * ソケット通信
   *
   */
  /**
   * サーバーにソケット接続
   */
  public connection(): void {
    this.websocketService.setRoom(this.roomname);
    this.websocketService.setGroup(this.groupname);
    this.websocketService.setName(this.username);
    this.websocketService.connection(this.websocketEvents);
  }
  /**
   * サーバーから切断
   */
  public disconnection(): void {
    this.websocketService.disconnect();
    this.webrtcService.deleteConnection(null, 'all');
    this.deleteRemoteVideoElement(null, 'all');
  }
  /**
   * ルーム設定
   * @param room string
   */
  public changeRoom(room: string): void {
    console.log(room);
    this.roomname = room;
  }

  public checkConnection(): boolean {
    return this.websocketService.getConnectStatus();
  }

  /**
   *
   * WebRTCヒデオ送信
   *
   */

  private setVideoElement(): void {
    this.videoElement = document.getElementById('myVideo');
    this.webrtcService.setVideoTarget(
      this.videoElement
    );
  }

  public videoSetup(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.setVideoElement();
      // navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || window.navigator.mozGetUserMedia;
      navigator.getUserMedia = navigator.getUserMedia;

      navigator.mediaDevices.getUserMedia({
        video: true,
        audio: false
      }).then((stream: MediaStream) => {
        // this.localStream = stream;
        this.webrtcService.setStream('local', stream);
        resolve(true);
      }).catch((error) => {
        console.log(error);
        reject(false);
      });
    });
  }
  public hideoOn(): void {
    this.videoSetup().then((result) => {
      if (result) {
        this.webrtcService.playVideo('local');
      }
    });
  }
  /**
   *
   * WebRTCヒデオ送信
   *
   */
  public hideoOff(): void {
    this.videoElement.pause();
    if (this.videoElement.src && (this.videoElement.src !== '') ) {
      window.URL.revokeObjectURL(this.videoElement.src);
    }
    this.videoElement.src = '';
  }

  public hideonSword(): void {
    this.websocketService.send(
      'webrtc',
      {
        'job': 'request_offer'
      });
  }

  public hideonGun(): void {
    this.webrtcService.hungUp();
  }

  public hideoOndemand(mode): void {
    this.webrtcService.setVideoMode(mode);
  }

  private webrtcManager(result): void {
    const mode = this.webrtcService.getVideoMode();
    if (result['job'] === 'send_sdp') {
      this.websocketService.send(
      'webrtc',
      {
        'job': 'remote_sdp',
        'data': result['data'],
        'to': result['id']
      });
    } else if (result['job'] === 'remote_sdp') {
        const data = JSON.parse(result['data']);
        if ('id' in result) {
          if (!this.remoteElement[result['id']]) {
            if (mode === 'mutual' || mode === 'reception') {
              const video = this.addRemoteVideoElement(result['id']);
              this.webrtcService.setRemoteVideoTarget(video, result['id']);
            }
          }
        }
        this.webrtcService.onSdpText(data, result['id']);
    } else if (result['job'] === 'request_offer') {
      console.log(mode);
      if (mode === 'mutual' || mode === 'delivery') {
        console.log('get offer request');
        if (this.webrtcService.checkAuthConnection(result['id'])) {
          this.webrtcService.makeOffer(result['id']);
          if (mode === 'mutual') {
            const video = this.addRemoteVideoElement(result['id']);
            this.webrtcService.setRemoteVideoTarget(video, result['id']);
          }
        }
      }
    }
  }

  private addRemoteVideoElement(id): void {
    const video = this.createVideoElement('remote_video_' + id);
    this.remoteElement[id] = video;
    return this.remoteElement[id];
  }

  private deleteRemoteVideoElement(id, type = 'id'): void {
    if (type === 'id') {
      this.removeVideoElement('remote_video_' + id);
      delete this.remoteElement[id];
    } else if (type === 'all') {
      for (const key in this.remoteElement) {
        if (this.remoteElement.hasOwnProperty(key)) {
          this.removeVideoElement('remote_video_' + key);
          delete this.remoteElement[key];
        }
      }
    }
  }

  private createVideoElement(id): any {
    const video = this.renderer2.createElement('video');
    video.id = id;
    video.setAttribute('class', 'hideon');

    this.videoBox.appendChild(video);
    return video;
  }

  private removeVideoElement(id): any {
    const video = document.getElementById(id);
    this.videoBox.removeChild(video);
    return video;
  }

}


