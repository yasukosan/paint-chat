import { Component, OnInit, Renderer2 } from '@angular/core';
import { MouseService, WebSocketService } from './service';
import { WebRTCService } from './service';
import { SubjectsService } from './service';
import { of } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  private videoElement: any;
  private remoteElement: any = {};
  private videoBox: any;

  messages: Array<any> = new Array();
  message: string;
  users: Array<string> = new Array();
  roomNumber: string;
  name: string;
  username: string = 'ゲスト';
  roomname: string = 'test1';
  groupname: string = 'test';

  websocketEvents: object = [
    'allusers', 'msg', 'draw', 'image', 'webrtc'
  ];

  canvasWidth = 700;
  canvasHeight = 400;
  canvasColorList = [
    '#E60012', '#F39800', '#FFF100', '#8FC31F', '#009944', '#009E96',
    '#00A0E9', '#0068B7', '#1D2088', '#920783', '#E4007F', '#E5004F',
    '#808080', '#000000', '#FFFFFF'
  ];

  canvasLineColor = '#555555';
  canvasLineCap = 'round';
  canvasLineWidth = 7;
  canvasAlpha = 1;

  private Reader: FileReader;
  Images = new Array();

  onDrag = false;

  showImage = false;
  imageData = '';

  private canvasID = 'artbox';
  private canvasBase: HTMLCanvasElement;
  private ctx;

  constructor(
    private renderer2: Renderer2,
    private mouseService: MouseService,
    private websocketService: WebSocketService,
    private webrtcService: WebRTCService,
    private subjectService: SubjectsService
  ) {}


  ngOnInit(): void {
    this.hub();
    this.setMouseEvent();
    this.videoBox = document.getElementById('videoBox');
  }


  private hub(): void {
    this.subjectService.on('on_msg')
      .subscribe((message: any) => {
        this.messages.push(message['name'] + ':' + message['msg']);
    });
    this.subjectService.on('on_draw')
      .subscribe((state: any) => {
        this.draw(state);
    });
    this.subjectService.on('on_image')
      .subscribe((image: any) => {
        this.Images.push(image.image);
    });

    this.subjectService.on('on_allusers')
      .subscribe((result: any) => {
        this.users = new Array<string>();
        console.log(result);
        for (const key in result) {
          if (result.hasOwnProperty(key)) {
            this.users.push(result[key]['name']);
          }
        }

      });
    this.subjectService.on('on_join')
    .subscribe((result: any) => {
      if (result['type'] === 'new_user') {
        this.subjectService.publish(
          'alert',
          result['data']['name'] + 'が参加しました'
        );
      }
    });
    this.subjectService.on('on_replicate')
      .subscribe((result: any) => {
    });
    this.subjectService.on('on_leave')
      .subscribe((result: any) => {
      this.deleteRemoteVideoElement(result['data']['id']);
      this.webrtcService.deleteConnection(result['data']['id']);
    });
    this.subjectService.on('on_webrtc')
      .subscribe((result: any) => {
        this.webrtcManager(result);
      });
  }

  public checkIronMan(): boolean {
    if (this.username === 'IRONMAN'
        || this.username === 'アイアンマン'
        || this.username === 'ironman') {
          return true;
    }
    return false;
  }

  /**
   *
   * 画像ドロップ
   *
   */
  public onDragOverHandler(event: DragEvent): void {
    event.preventDefault();
    this.onDrag = true;
  }
  public onDragLeaveHandler(event: DragEvent): void {
    event.stopPropagation();
    this.onDrag = false;
  }
  public onSelectHandler(event: DragEvent): void {
    this.onDrag = false;
    event.preventDefault();
    const file = event.dataTransfer.files;

    if (file[0] || file[0].type.indexOf('image/') > 0) {
      this.Reader = new FileReader();
      this.Reader.onloadend = (e: any) => {
        const image = e.target.result;
        this.Images.push(image);
        this.sendImage(image, 'send');
        e.stopPropagation();
      };
      this.Reader.readAsDataURL(file[0]);
    }
  }

  public imageReview(id: number = 0): void {
    if  ( id === 0 && this.showImage) {
      this.imageData = '';
      this.showImage = false;
    } else {
      this.imageData = this.Images[id];
      this.showImage = true;
    }
  }

  /**
   *
   * お絵かき
   *
   */
  private setMouseEvent(): void {
    this.canvasBase = <HTMLCanvasElement> document.getElementById(this.canvasID);
    this.ctx = this.canvasBase.getContext('2d');
    this.ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);

    const rect = this.canvasBase.getBoundingClientRect();
    this.mouseService.setCorrection(rect);

    this.canvasBase.addEventListener('mousedown', (e: MouseEvent) => {
      this.mouseService.setStartPosition(e);
    });
    this.canvasBase.addEventListener('mouseup', (e) => {
      this.mouseService.end();
    });
    this.canvasBase.addEventListener('mousemove', (e: MouseEvent) => {
      this.mouseMoveJob(e);
    });
    this.canvasBase.addEventListener('touchstart', (e) => {
      this.mouseService.setStartPosition(e);
    });
    this.canvasBase.addEventListener('touchend', (e) => {
      this.mouseService.end();
    });
    this.canvasBase.addEventListener('touchmove', (e) => {
      this.mouseMoveJob(e);
    });
  }
  private mouseMoveJob(e): void {
    if (this.mouseService.getMoveFlag()) {
      this.mouseService.mouseMove(e);
      const position = this.buildDrawStatus(this.mouseService.getMousePosition());
      this.draw(position);
      this.sendDraw(position);
    }
  }


  /**
   * キャンバスに絵を書く
   * @param mouse_position array
   */
  private draw(mouse_position): void {

    this.ctx.beginPath();
    this.ctx.moveTo(
      mouse_position['startx'], mouse_position['starty']
    );
    this.ctx.lineTo(
      mouse_position['movex'], mouse_position['movey']
    );
    this.ctx.lineCap = mouse_position['linecap'];
    this.ctx.lineWidth = mouse_position['linewidth'];
    this.ctx.strokeStyle = mouse_position['linecolor'];
    this.ctx.stroke();
  }

  /**
   * キャンバスに描く内容と描画オプションを結合
   * @param position object
   */
  private buildDrawStatus(position: object): object {
    const options = {
      linecap: this.canvasLineCap,
      linewidth: this.canvasLineWidth,
      linealpha: this.canvasAlpha,
      linecolor: this.canvasLineColor
    };
    return Object.assign(position, options);
  }

  /**
   * ペイント色の変更
   * @param color string
   */
  public setPaintColor(color: string): void {
    this.canvasLineColor = color;
  }

  /**
   *
   * ソケット通信
   *
   */
  /**
   * サーバーにソケット接続
   */
  public connection(): void {
    this.websocketService.setRoom(this.roomname);
    this.websocketService.setGroup(this.groupname);
    this.websocketService.setName(this.username);
    this.websocketService.connection(this.websocketEvents);
  }
  /**
   * サーバーから切断
   */
  public disconnection(): void {
    this.websocketService.disconnect();
    this.webrtcService.deleteConnection(null, 'all');
    this.deleteRemoteVideoElement(null, 'all');
  }
  /**
   * ルーム設定
   * @param room string
   */
  public changeRoom(room: string): void {
    console.log(room);
    this.roomname = room;
  }
  /**
   * メッセージ送信
   * @param message string
   */
  public send(message: string): void {
    this.messages.push(this.username + ':' + message);
    this.websocketService.send(
      'msg', {
        msg:  message,
        name: this.username
      });
  }
  /**
   * お絵かき情報送信
   * @param draw array
   */
  public sendDraw(draw: object): void {
    this.websocketService.send('draw', draw);
  }
  /**
   * 画像送信
   * @param image array
   */
  public sendImage(image: object, job: string = 'send'): void {
    this.websocketService.send(
      'image', {
        image : image,
        job   : job
      });
  }
  /**
   * クライアント同期データの送信
   */
  public replicateClient(): void {
    this.websocketService.send(
      '',
      {}
    );
  }

  public checkConnection(): boolean {
    return this.websocketService.getConnectStatus();
  }

  /**
   *
   * WebRTCヒデオ送信
   *
   */

  private setVideoElement(): void {
    this.videoElement = document.getElementById('myVideo');
    this.webrtcService.setVideoTarget(
      this.videoElement
    );
  }

  public videoSetup(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.setVideoElement();
      // navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || window.navigator.mozGetUserMedia;
      // navigator.getUserMedia = navigator.getUserMedia;

      navigator.mediaDevices.getUserMedia({
        video: true,
        // video: {facingMode: 'user'},
        audio: false
      }).then((stream: MediaStream) => {
        // this.localStream = stream;
        this.webrtcService.setStream('local', stream);
        resolve(true);
      }).catch((error) => {
        console.log(error);
        reject(false);
      });
    });
  }
  public hideoOn(): void {
    this.videoSetup().then((result) => {
      if (result) {
        this.webrtcService.playVideo('local');
      }
    });
  }

  public hideoOff(): void {
    this.videoElement.pause();
    if (this.videoElement.src && (this.videoElement.src !== '') ) {
      window.URL.revokeObjectURL(this.videoElement.src);
    }
    this.videoElement.src = '';
  }

  public hideonSword(): void {
    this.websocketService.send(
      'webrtc',
      {
        'job': 'request_offer'
      });
  }

  public hideonGun(): void {
    this.webrtcService.hungUp();
  }

  public hideoOndemand(mode): void {
    this.webrtcService.setVideoMode(mode);
  }

  public checkHideoOndemand(mode): string {
    if (this.webrtcService.getVideoMode() === mode) {
      return 'rgba(150,150,150,1)';
    } else {
      return '';
    }
  }

  private webrtcManager(result): void {
    const mode = this.webrtcService.getVideoMode();
    if (result['job'] === 'send_sdp') {
      this.websocketService.send(
      'webrtc',
      {
        'job': 'remote_sdp',
        'data': result['data'],
        'to': result['id']
      });
    } else if (result['job'] === 'remote_sdp') {
      const data = JSON.parse(result['data']);
      if ('id' in result) {
        if (this.webrtcService.checkAuthConnection(result['id'] === true)
        && this.webrtcService.checkMode(['mutual', 'reception'])) {
            this.addVideoElement(result['id']);
        }
        console.log(data);
      }
      this.webrtcService.onSdpText(data, result['id']);
    } else if (
        result['job'] === 'request_offer'
        && this.webrtcService.checkMode(['mutual', 'delivery'])) {

      console.log('get offer request');
      if (this.webrtcService.checkAuthConnection(result['id'])) {
        this.webrtcService.makeOffer(result['id']);
        if (mode === 'mutual') {
          this.addVideoElement(result['id']);
        }
      }
    }
  }

  private addVideoElement(id): void {
    if (!this.remoteElement[id]) {
      const video = this.addRemoteVideoElement(id);
      this.webrtcService.setRemoteVideoTarget(video, id);
    }
  }

  private addRemoteVideoElement(id): void {
    const video = this.createVideoElement('remote_video_' + id);
    this.remoteElement[id] = video;
    return this.remoteElement[id];
  }

  private deleteRemoteVideoElement(id, type = 'id'): void {
    if (type === 'id') {
      this.removeVideoElement('remote_video_' + id);
      delete this.remoteElement[id];
    } else if (type === 'all') {
      for (const key in this.remoteElement) {
        if (this.remoteElement.hasOwnProperty(key)) {
          this.removeVideoElement('remote_video_' + key);
          delete this.remoteElement[key];
        }
      }
    }
  }

  private createVideoElement(id): any {
    const video = this.renderer2.createElement('video');
    video.id = id;
    video.setAttribute('class', 'hideon');

    this.videoBox.appendChild(video);
    return video;
  }

  private removeVideoElement(id): any {
    const video = document.getElementById(id);
    this.videoBox.removeChild(video);
    return video;
  }


  /**
   * 録画再生
   */

  public start_recorde(id): void {
    const hideo = document.getElementById(id);
    this.webrtcService.setRecordePlayer(hideo);
    this.webrtcService.startRecord();
  }

  public stop_recorde(): void {
    this.webrtcService.stopRecord();
  }

  public play_recorde(): void {
    this.webrtcService.plyaRecord();
  }

  public dl_recorde(id): void {
    const dl: any = document.getElementById(id);
    dl.download = 'hideo.webm';
    dl.href = this.webrtcService.getRecordeURL();
  }

}


