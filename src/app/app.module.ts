import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { VideoComponent } from './video/video.component';

import { AlertComponent, ConfirmationComponent, LoadingComponent } from './_lib_component';

import { MessageService } from './message/message.service';
import { WebSocketService, WebRTCService } from './service';
import { MouseService } from './service';

import { SubjectsService } from './service';

@NgModule({
  declarations: [
    AppComponent,
    VideoComponent,
    AlertComponent,
    ConfirmationComponent,
    LoadingComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    SubjectsService,
    MessageService,
    WebSocketService,
    WebRTCService,
    MouseService
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
